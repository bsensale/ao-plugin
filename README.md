# Active Object Plugin

## Description

Active Objects is an ORM (object relational mapping) layer in Atlassian products. This plugin integrates
[Active Objects](https://bitbucket.org/activeobjects/ao/) into the products.

## Atlassian Developer?

### Internal Documentation

[Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/AO/Home)

### Committing Guidelines

Please see [the following guide](https://extranet.atlassian.com/x/Uouvdg) for committing to this module.

### Builds

The Bamboo builds for this project are on [BEAC](https://extranet-bamboo.internal.atlassian.com/browse/AO)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/AO)

### Documentation

[Active Objects Documentation](https://developer.atlassian.com/display/DOCS/Active+Objects)
