package com.atlassian.activeobjects.spi;

/**
 * Represents the type of database.
 */
public enum DatabaseType
{
    HSQL,
    MYSQL,
    POSTGRESQL,
    ORACLE,
    MS_SQL,
    NUODB,
    DB2,
    DERBY_EMBEDDED,
    DERBY_NETWORK,
    UNKNOWN
}
